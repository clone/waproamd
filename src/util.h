#ifndef fooutilhfoo
#define fooutilhfoo

/* $Id$ */

/*
 * This file is part of waproamd.
 *
 * waproamd is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * waproamd is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with waproamd; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
 */

#include <stdio.h>
#include <stdint.h>

#ifndef ETH_ALEN
#define ETH_ALEN 6
#endif

#ifndef MIN
#define MIN(a,b) ((a)<(b)?(a):(b))
#endif

struct hw_addr {
    uint8_t addr[ETH_ALEN];
};

extern struct hw_addr null_ap;

void print_hex(FILE *f, uint8_t *w, int l);
void print_hw_addr(FILE*f, struct hw_addr *a);
void snprint_hw_addr(char *c, int l, struct hw_addr *a);
int parse_hex(char *s, uint8_t *b, int l);
int hw_addr_equal(const struct hw_addr *a, const struct hw_addr *b);
int is_assoc_ap(const struct hw_addr *ap);
int get_ifname(int idx, char *p, int l);
int is_iface_available(const char *p);
const char* escape_essid(const char *s);

#endif
