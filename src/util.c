/* $Id$ */

/*
 * This file is part of waproamd.
 *
 * waproamd is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * waproamd is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with waproamd; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
 */

#include <unistd.h>
#include <sys/socket.h>
#include <string.h>
#include <assert.h>
#include <errno.h>
#include <sys/ioctl.h>
#include <linux/if.h>
#include <libdaemon/dlog.h>

#include "util.h"
#include "iwapi.h"

struct hw_addr null_ap = { { 0, 0, 0, 0, 0, 0 } };

void print_hex(FILE *f, uint8_t *w, int l) {
    while (l > 0) {
        fprintf(f, "%02x", *(w++));
        l--;
    }
}

int hw_addr_equal(const struct hw_addr *a, const struct hw_addr *b) {
    return memcmp(a->addr, b->addr, ETH_ALEN) == 0;
}

int is_assoc_ap(const struct hw_addr *ap) {
    int b, j;
    b = 1;
    assert(ap);
    
    for (j = 1; j < ETH_ALEN; j++)
        if (ap->addr[j] != ap->addr[0]) {
            b = 0;
            break;
        }

    return !b || (ap->addr[0] != 0xFF && ap->addr[0] != 0x44 && ap->addr[0] != 0x00);
}


void print_hw_addr(FILE*f, struct hw_addr *a) {
    fprintf(f, "%02x:%02x:%02x:%02x:%02x:%02x",
           a->addr[0], a->addr[1], a->addr[2],
           a->addr[3], a->addr[4], a->addr[5]);
}

void snprint_hw_addr(char *c, int l, struct hw_addr *a) {
    snprintf(c, l, "%02x:%02x:%02x:%02x:%02x:%02x",
             a->addr[0], a->addr[1], a->addr[2],
             a->addr[3], a->addr[4], a->addr[5]);
}


int parse_hex(char *s, uint8_t *b, int l) {
    int n = 0;
    char *p;
    int nibble = 0;
    
    for (p = s; *p && l >= 0; p++) {
        int c;

        if (*p >= '0' && *p <= '9')
            c = *p - '0';
        else if (*p >= 'A' && *p <= 'F')
            c = *p - 'A' + 10;
        else if (*p >= 'a' && *p <= 'f')
            c = *p - 'a' + 10;
        else if (*p == ':' || *p == '.')
            continue;
        else
            return -(p-s)-1;

        if (!nibble)
            *b = c << 4;
        else {
            *b |= c & 15;
            b++;
            l--;
            n++;
        }

        nibble = !nibble;
    }

    if (*p)
        return -(p-s)-1;
    
    return n;
}


int get_ifname(int idx, char *p, int l) {
    struct ifreq req;
    int s;

    if ((s = socket(PF_INET, SOCK_DGRAM, 0)) < 0) {
        daemon_log(LOG_ERR, "socket() failed: %s\n", strerror(errno));
        return -1;
    }
    
    memset(&req, 0, sizeof(req));
    req.ifr_ifindex = idx;
    
    if(ioctl(s, SIOCGIFNAME, &req) < 0) {
        close(s);
        daemon_log(LOG_ERR,  "SIOCGIFNAME failed: %s\n", strerror(errno));
        return -1;
    }

    close(s);

    strncpy(p, req.ifr_name, l-1);
    p[l-1] = 0;

    return 0;
}

int is_iface_available(const char *p) {
    struct ifreq req;
    int s, r;

    if ((s = socket(PF_INET, SOCK_DGRAM, 0)) < 0) {
        daemon_log(LOG_ERR, "socket() failed: %s\n", strerror(errno));
        return -1;
    }
    
    memset(&req, 0, sizeof(req));
    strncpy(req.ifr_name, p, IFNAMSIZ);
    
    if ((r = ioctl(s, SIOCGIFINDEX, &req)) < 0 && errno != ENODEV) {
        daemon_log(LOG_ERR, "SIOCGIFINDEX failed: %s\n", strerror(errno));
        return -1;
    }

    close(s);
    return r >= 0 && req.ifr_ifindex >= 0;
}

const char* escape_essid(const char *s) {
    static const char hextab[] = "0123456789ABCDEF";
    static char output[3*IW_ESSID_MAX_SIZE + 1];
    const char *i;
    char *o;

    for (i = s, o = output; i-s < IW_ESSID_MAX_SIZE && *i; i++) {

        if (*i >= 32 && *i <= 126 && *i != '/' && *i != '%')
            *(o++) = *i;
        else {
            *(o++) = '%';
            *(o++) = hextab[*i >> 4];
            *(o++) = hextab[*i & 0xF];
        }
    }

    *(o++) = 0;
    
    return output;
}
