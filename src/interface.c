/* $Id$ */

/*
 * This file is part of waproamd.
 *
 * waproamd is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * waproamd is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with waproamd; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
 */

#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <netinet/in.h>

#include <libdaemon/dlog.h>

#include "interface.h"
#include "util.h"
#include "wireless.h"

struct interface *interface_open(char *name) {
    struct interface* i = NULL;

    if (!(i = (struct interface*) malloc(sizeof(struct interface))))
        return NULL;

    memset(i, 0, sizeof(struct interface));
    strncpy(i->name, name, IFNAMSIZ);

    if ((i->fd = socket(PF_INET, SOCK_DGRAM, 0)) < 0) {
        daemon_log(LOG_ERR, "socket(): %s\n", strerror(errno));
        goto fail;
    }
        
    return i;
    
fail:

    if (i)
        interface_close(i);

    return NULL;
}

void interface_close(struct interface *i) {
    assert(i);

    if (i->fd >= 0)
        close(i->fd);

    free(i);
}


void interface_up(struct interface *i) {
    struct ifreq ifr;
    assert(i && i->fd >= 0 && i->name);
    
    memset(&ifr, 0, sizeof(ifr));
    strncpy(ifr.ifr_name, i->name, sizeof(ifr.ifr_name)-1);
    
    if (ioctl(i->fd, SIOCGIFFLAGS, &ifr) < 0) {
        daemon_log(LOG_WARNING, "Warning: Could not get interface flags.");
        return;
    }

    if ((ifr.ifr_flags & IFF_UP) == IFF_UP)
        return;
    
    if (ioctl(i->fd, SIOCGIFADDR, &ifr) < 0)
        daemon_log(LOG_WARNING, "Warning: Could not get interface address.");
    else if (ifr.ifr_addr.sa_family != AF_INET)
        daemon_log(LOG_WARNING, "Warning: The interface is not IP-based.");
    else {
        ((struct sockaddr_in *)(&ifr.ifr_addr))->sin_addr.s_addr = INADDR_ANY;
        if (ioctl(i->fd, SIOCSIFADDR, &ifr) < 0)
            daemon_log(LOG_WARNING, "Warning: Could not set interface address.");
    }

    if (ioctl(i->fd, SIOCGIFFLAGS, &ifr) < 0) {
        daemon_log(LOG_WARNING, "Warning: Could not get interface flags.");
        return;
    }
    
    ifr.ifr_flags |= IFF_UP;
    
    if (ioctl(i->fd, SIOCSIFFLAGS, &ifr) < 0)
        daemon_log(LOG_WARNING, "Warning: Could not set interface flags.");

    daemon_log(LOG_INFO, "Interface set to status UP.");
}
