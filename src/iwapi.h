#ifndef fooiwapihfoo
#define fooiwapihfoo

/* $Id$ */

/*
 * This file is part of waproamd.
 *
 * waproamd is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * waproamd is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with waproamd; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
 */

#include "interface.h"
#include "util.h"
#include "wireless.h"

struct ap_info {
    struct hw_addr ap;
    struct iw_freq freq;
    char essid[IW_ESSID_MAX_SIZE + 1];
};

int iw_set_essid(struct interface *i, const char* essid);
int iw_set_mode(struct interface *i, int m);
/*int iw_set_freq(struct interface *i, const struct iw_freq *f);*/
int iw_set_ap(struct interface *i, const struct hw_addr *ap);

/* Size of string essid must be at least IW_ESSID_MAX_SIZE + 1 characters long */
int iw_get_essid(struct interface *i, char *essid);
int iw_get_mode(struct interface *i, int *m);
int iw_get_freq(struct interface *i, struct iw_freq *f);
int iw_get_ap(struct interface *i, struct hw_addr *ap);

int iw_scan(struct interface *i);
int iw_scan_result(struct interface *i, int (*callback)(struct ap_info*));

int iw_tune(struct interface *i, struct ap_info *ap);

/* Check whether the card is associated and fill in info record for associated AP */
int iw_assoc(struct interface *i, struct ap_info *ap);

int iw_ap_info_equal(const struct ap_info *a, const struct ap_info *b);

#endif
