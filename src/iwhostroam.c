/* $Id$ */

/*
 * This file is part of waproamd.
 *
 * waproamd is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * waproamd is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with waproamd; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
 */

#include <assert.h>
#include <string.h>
#include <sys/ioctl.h>
#include <libdaemon/dlog.h>
#include <errno.h>

#include "iwapi.h"
#include "wireless.h"

#define IW_MAX_PRIV_DEF 128

int iw_set_hostroam(struct interface *i, int h) {
    struct iwreq req;
    struct iw_priv_args pa[IW_MAX_PRIV_DEF];
    int j, subcmd = 0, b_subcmd = 0;

    assert(i);

    memset(&req, 0, sizeof(req));
    strncpy(req.ifr_ifrn.ifrn_name, i->name, IFNAMSIZ);
    
    req.u.data.pointer = (caddr_t) pa;
    req.u.data.length = IW_MAX_PRIV_DEF;
    req.u.data.flags = 0;

    if(ioctl(i->fd, SIOCGIWPRIV, &req) < 0)
        return -1;
    
    if (req.u.data.length <= 0)
        return -1;

    for (j = 0; j < req.u.data.length; j++)
        if (!strcmp(pa[j].name, "host_roaming"))
            break;
    
    if (j >= req.u.data.length)
        return -1;

    if ((pa[j].set_args & IW_PRIV_TYPE_MASK) != IW_PRIV_TYPE_INT ||
        (pa[j].set_args & IW_PRIV_SIZE_MASK) != 1) {
        daemon_log(LOG_WARNING, "Private ioctl host_roaming with bogus arguments found!");
        return -1;
    }

    /* Look for sub ioctls */
    if(pa[j].cmd < SIOCDEVPRIVATE) {
      int k;
 
      for (k = 0; k < req.u.data.length; k++) {
          if (pa[k].name[0] == 0 &&
              pa[j].set_args == pa[k].set_args &&
              pa[j].get_args == pa[k].get_args)
              break;
      }

      if (k >= req.u.data.length) {
          daemon_log(LOG_WARNING, "Invalid private ioctl definition!");
          return -1;
      }
          
      subcmd = pa[j].cmd;
      b_subcmd = 1;
      j = k;
    }

    if (h < 0) h = 0;
    if (h > 2) h = 2;
    
    memset(&req, 0, sizeof(req));
    strncpy(req.ifr_ifrn.ifrn_name, i->name, IFNAMSIZ);
    req.u.data.length = 1;
    req.u.mode = b_subcmd ? subcmd : 0;
    memcpy(req.u.name+4, &h, sizeof(h));
        
    if (ioctl(i->fd, pa[j].cmd, &req) < 0) {
        daemon_log(LOG_WARNING, "Private ioctl host_roaming (%x) failed: %s", pa[j].cmd, strerror(errno));
        return -1;
    }

    daemon_log(LOG_INFO, "Successfully set host roaming of driver to <%s>", !h ? "hardware" : (h == 1 ? "host" : "user space"));
    return 0;
}
